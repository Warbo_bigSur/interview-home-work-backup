import React from "react";


function Post (props){
  
    return  (
        <div  >
        <h2>{props.title}</h2>
        <p className="author-post">Author: {props.user}</p>
        <p>Created at: Sep 20, 2021 </p>
        <p className="text-left">{props.content}</p>
        <hr className="comment-line" />
        
        <hr />
        </div>)
        
}
export default Post