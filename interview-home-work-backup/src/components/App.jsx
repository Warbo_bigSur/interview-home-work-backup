import React from 'react';
import Post from './Post';
import Comments from './Comments';

const getPostData = [];
const getCommentData = [];
const urlPost = 'https://jsonplaceholder.typicode.com/posts';
fetch(urlPost).then((response) => response.json())
    .then(function (data) {
        getPostData.push(...data)
        console.log(typeof data)
    })
    .catch((error) => console.log(error));

const urlComments = 'https://jsonplaceholder.typicode.com/comments';
fetch(urlComments).then((response) => response.json())
    .then(function (dataComment) {
        getCommentData.push(...dataComment)
        console.log(getCommentData)
        console.log(typeof getCommentData)
    })
    .catch((error) => console.log(error));

const urlUser = 'https://jsonplaceholder.typicode.com/users';
fetch(urlUser).then((response) => response.json())
    .then(function (dataUser) {
        getPostData.push(...dataUser)
        console.log(dataUser)
        console.log(typeof dataUser)
    })
    .catch((error) => console.log(error));


function App() {
    return (<div>
        {getPostData.map((postItem) => {
            return (
                <Post
                    key={postItem.id}
                    user={postItem.name}
                    userId={postItem.userId}
                    title={postItem.title}
                    content={postItem.body}
                />
            )

        })};
        {getCommentData.map((commentItem) => {
            return (
                <Comments
                    postId={commentItem.postId}
                    id={commentItem.id}
                    name={commentItem.name}
                    email={commentItem.email}
                    content={commentItem.body}
                />
            )

        })}

    </div>
    )
}
export default App